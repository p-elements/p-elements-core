/// <reference types="underscore" />
/// <reference types="@types/animejs" />

declare interface IElementConfig {
  tagName: string;
  options?: {
    extends: string;
  };
}

declare interface TransitionStrategy {
  enter(
    element: Element,
    properties: VNodeProperties,
    enterAnimation: string
  ): void;
  exit(
    element: Element,
    properties: VNodeProperties,
    exitAnimation: string,
    removeElement: () => void
  ): void;
}

declare interface ProjectorOptions {
  readonly transitions?: TransitionStrategy;
  styleApplyer?(domNode: HTMLElement, styleName: string, value: string): void;
}

declare interface ProjectionOptions extends ProjectorOptions {
  readonly namespace?: string;
  eventHandlerInterceptor?: (
    propertyName: string,
    eventHandler: Function,
    domNode: Node,
    properties: VNodeProperties
  ) => Function;
}

declare interface EventHandler {
  (ev?: Event): boolean | void;
}

declare interface VNodeProperties {
  enterAnimation?:
    | ((element: Element, properties?: VNodeProperties) => void)
    | string;
  exitAnimation?:
    | ((
        element: Element,
        removeElement: () => void,
        properties?: VNodeProperties
      ) => void)
    | string;
  updateAnimation?: (
    element: Element,
    properties?: VNodeProperties,
    previousProperties?: VNodeProperties
  ) => void;
  afterCreate?(
    element: Element,
    projectionOptions: ProjectionOptions,
    vnodeSelector: string,
    properties: VNodeProperties,
    children: VNode[]
  ): void;
  afterUpdate?(
    element: Element,
    projectionOptions: ProjectionOptions,
    vnodeSelector: string,
    properties: VNodeProperties,
    children: VNode[]
  ): void;
  afterRemoved?(element: Element): void;
  readonly bind?: Object;
  readonly key?: Object;
  readonly classes?: { [index: string]: boolean | null | undefined };
  readonly styles?: { [index: string]: string | null | undefined };
  
  readonly on?: {
    [eventName: string]:
      | EventHandler
      | {
          listener: EventHandler;
          options: { capture?: boolean; passive?: boolean; once?: boolean };
        };
  };

  readonly onCapture?: { [eventName: string]: EventHandler };

  readonly is?: string;

  ontouchcancel?(ev?: TouchEvent): boolean | void;
  ontouchend?(ev?: TouchEvent): boolean | void;
  ontouchmove?(ev?: TouchEvent): boolean | void;
  ontouchstart?(ev?: TouchEvent): boolean | void;

  readonly action?: string;
  readonly encoding?: string;
  readonly enctype?: string;
  readonly method?: string;
  readonly name?: string;
  readonly target?: string;

  readonly href?: string;
  readonly rel?: string;

  onblur?(ev: FocusEvent): boolean | void;
  onchange?(ev: Event): boolean | void;
  onclick?(ev: MouseEvent): boolean | void;
  ondblclick?(ev: MouseEvent): boolean | void;
  ondrag?(ev: DragEvent): boolean | void;
  ondragend?(ev: DragEvent): boolean | void;
  ondragenter?(ev: DragEvent): boolean | void;
  ondragleave?(ev: DragEvent): boolean | void;
  ondragover?(ev: DragEvent): boolean | void;
  ondragstart?(ev: DragEvent): boolean | void;
  ondrop?(ev: DragEvent): boolean | void;
  onfocus?(ev: FocusEvent): boolean | void;
  oninput?(ev: Event): boolean | void;
  onkeydown?(ev: KeyboardEvent): boolean | void;
  onkeypress?(ev: KeyboardEvent): boolean | void;
  onkeyup?(ev: KeyboardEvent): boolean | void;
  onload?(ev: Event): boolean | void;
  onmousedown?(ev: MouseEvent): boolean | void;
  onmouseenter?(ev: MouseEvent): boolean | void;
  onmouseleave?(ev: MouseEvent): boolean | void;
  onmousemove?(ev: MouseEvent): boolean | void;
  onmouseout?(ev: MouseEvent): boolean | void;
  onmouseover?(ev: MouseEvent): boolean | void;
  onmouseup?(ev: MouseEvent): boolean | void;
  onmousewheel?(ev: WheelEvent): boolean | void;
  onscroll?(ev: UIEvent): boolean | void;
  onsubmit?(ev: Event): boolean | void;
  onpointercancel?(ev: PointerEvent): boolean | void;
  onpointerdown?(ev: PointerEvent): boolean | void;
  onpointerenter?(ev: PointerEvent): boolean | void;
  onpointerleave?(ev: PointerEvent): boolean | void;
  onpointermove?(ev: PointerEvent): boolean | void;
  onpointerout?(ev: PointerEvent): boolean | void;
  onpointerover?(ev: PointerEvent): boolean | void;
  onpointerup?(ev: PointerEvent): boolean | void;

  readonly spellcheck?: boolean;
  readonly tabIndex?: number;
  readonly disabled?: boolean;
  readonly title?: string;
  readonly accessKey?: string;
  readonly id?: string;

  readonly type?: string;
  readonly autocomplete?: string;
  readonly checked?: boolean;
  readonly placeholder?: string;
  readonly readOnly?: boolean;
  readonly src?: string;
  readonly value?: string;

  readonly alt?: string;
  readonly srcset?: string;
  readonly innerHTML?: string;

  readonly [index: string]: any;
}

declare interface VNode {
  readonly vnodeSelector: string;
  readonly properties: VNodeProperties | undefined;
  readonly children: Array<VNode> | undefined;
  readonly text: string | undefined;
  domNode: Node | null;
}

declare interface ProjectionOptions extends ProjectorOptions {
  readonly namespace?: string;
  eventHandlerInterceptor?: (
    propertyName: string,
    eventHandler: Function,
    domNode: Node,
    properties: VNodeProperties
  ) => Function;
}

declare interface Projection {
  readonly domNode: Element;
  update(updatedVnode: VNode): void;
}

declare interface Projector {
  append(parentNode: Element, renderMaquetteFunction: () => VNode): void;
  insertBefore(beforeNode: Element, renderMaquetteFunction: () => VNode): void;
  merge(domNode: Element, renderMaquetteFunction: () => VNode): void;
  replace(domNode: Element, renderMaquetteFunction: () => VNode): void;
  resume(): void;
  scheduleRender(): void;
  renderNow(): void;
  detach(renderMaquetteFunction: () => VNode): Projection;
  stop(): void;
}

declare interface Mapping<Source, Target> {
  results: Array<Target>;
  map(newSources: Array<Source>): void;
}

declare interface CalculationCache<Result> {
  invalidate(): void;
  result(inputs: Object[], calculation: () => Result): Result;
}

declare const CustomElementConfig: (
  config: IElementConfig
) => (Element: any) => void;

declare type ElementProjectorMode = "append" | "merge" | "replace";

interface PropertyOptions {
  type?: PropertyType;
  reflect?: boolean;
  attribute?: string;
  readonly?: boolean;
  converter?: AttributeConverter<any>;
}
interface PropertyInfo extends PropertyOptions {
  name: string;
}

declare abstract class CustomElement extends HTMLElement {
  constructor(self?: any);
  get internals(): ElementInternals;
  get properties(): readonly PropertyInfo[];
  protected addStylesheetToRootNode(
    style: string,
    rootNode: ShadowRoot | Document
  ): void;
  protected templateFromString(
    template: string,
    useShadowRoot?: boolean
  ): DocumentFragment;
  protected adoptStyle(root: Document | ShadowRoot, css: string): string | void;
  protected createProjector(
    element: Element,
    render: () => VNode
  ): Promise<Projector>;
  addController(controller: CustomElementController): void;
  renderNow(): void;
  scheduleRender(): void;
  connectedCallback(): void;
  disconnectedCallback(): void;
  updated?(propertyName: string, oldValue: any, newValue: any): void;
  shouldUpdate?(propertyName: string, oldValue: any, newValue: any): boolean;
  attributeChangedCallback?(
    name: string,
    oldValue: string | null,
    newValue: string | null
  ): void;
}

declare abstract class CustomElementController {
  constructor(hostElement: CustomElement);
  hostElement: CustomElement;
  renderNow(): void;
  init?(): void;
  connected?(): void;
  disconnected?(): void;
}

declare const Bind: (
  target: object,
  propertyKey: string,
  descriptor: any
) => void;

declare const PropertyRenderOnSet: (
  target: object,
  propertyKey: string
) => void;

declare const RenderOnSet: (target: object, propertyKey: string) => void;

declare const Query: (
  selector: string
) => (target: object, propertyKey: string) => void;

interface AttributeConverter<T> {
  fromAttribute?(value: string | null): T;
  toAttribute?(value: T): string;
}
declare type PropertyType = "string" | "number" | "boolean" | "object";

declare const Property: (options?: {
  attribute?: string;
  type: PropertyType;
  reflect?: boolean;
  readonly?: boolean;
  converter?: AttributeConverter<any>;
}) => (target: object, propertyKey: string) => void;

declare const Maquette: {
  createCache: <Result>() => CalculationCache<Result>;
  createProjector: (projectorOptions?: ProjectorOptions) => Projector;
  createMapping: <Source, Target>(
    getSourceKey: (source: Source) => string | number,
    createResult: (source: Source, index: number) => Target,
    updateResult: (source: Source, target: Target, index: number) => void
  ) => Mapping<Source, Target>;
  dom: {
    append: (vnode: VNode, projectionOptions?: ProjectionOptions) => Projection;
    create: (vnode: VNode, projectionOptions?: ProjectionOptions) => Projection;
    insertBefore: (
      vnode: VNode,
      projectionOptions?: ProjectionOptions
    ) => Projection;
    merge: (vnode: VNode, projectionOptions?: ProjectionOptions) => Projection;
    replace: (
      vnode: VNode,
      projectionOptions?: ProjectionOptions
    ) => Projection;
  };
  h: (selector: string) => VNode;
};

declare const _: _.UnderscoreStatic;