const path = require("path");
const webpack = require('webpack');
const TerserPlugin = require("terser-webpack-plugin")
const package = require('./package.json');
const banner = `P-ELEMENTS ${package.version} - ${new Date()}`;

module.exports = {
  devServer: {
    hot: false,
    inline: false,
    liveReload: false,
    injectClient: false,
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          format: {
            comments: /^\**!/i,
          },
        },
        extractComments: false,
      }),
    ],
  },
  plugins: [
    new webpack.BannerPlugin({banner}),
  ],
  entry: {
    "dist/p-elements-core": [
      "@webreflection/custom-elements-builtin",
      "underscore",
      "element-internals-polyfill",
      "./src/p-elements-core.ts",
    ],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: ["babel-loader", "ts-loader"],
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  output: {
    path: path.resolve(__dirname),
    filename: "[name]" + ".js",
    chunkFilename: "[chunkhash]" + ".js",
  }
};
