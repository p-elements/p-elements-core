import { PropertyInfo } from "./decorators/property";
import { replaceApplyToCssVars } from "./helpers/css";
import { ICustomElementController } from "./custom-element-controller";
import { Projector, VNode } from "./maquette/interfaces";

export type ElementProjectorMode = "append" | "merge" | "replace";

declare var HTMLElement: {
  prototype: HTMLElement;
  new (param?): HTMLElement;
};

declare var HTMLElement: {
  prototype: HTMLElement;
  new (arg?: any): HTMLElement;
};

const documentAdoptedStyleSheets: number[] = [];

export abstract class CustomElement extends HTMLElement {
  constructor(self?: any) {
    super();
    this.#init();
    this.#callInitFunction();
    return self;
  }

  #properties: PropertyInfo[];

  #projector: Projector;

  #projectorMode: ElementProjectorMode;

  #cssText: string;

  #cssSheet: CSSStyleSheet;

  #linkElement: HTMLLinkElement;

  #isSheetAdopted = false;

  #useShadowRoot;

  #delegatesFocus;

  #internals: ElementInternals;

  #controllers :any[] = [];

  #connected = false;

  #canReflect = false;

  get _canReflect(): boolean {
    return this.#canReflect;
  }

  get properties() : readonly PropertyInfo[] {
    return this.#properties;
  } 

  addController(controller: ICustomElementController) {
    this.#controllers.push(controller);
    if (this.#connected && controller.connected) {
      controller.connected();
    }
  }

  #callInitFunction() {
    if (typeof (this as any).init === "function") {
        (this as any).init();
        this.#controllers.forEach((controller) => {
          controller?.init();
      });
    }
  }

  #init() {
    this.#initProperties();
    const staticProjectorMode = (this.constructor as any).projectorMode;
    this.#projectorMode = staticProjectorMode ? staticProjectorMode : "append";
    const isFormAssociated = (this.constructor as any).formAssociated;
    const delegatesFocus = (this.constructor as any).delegatesFocus;
    this.#delegatesFocus = delegatesFocus;
    if (isFormAssociated) {
      this.#internals = this.attachInternals();
    }
    const css = (this.constructor as any).style;

    if (css) {
      this.#useShadowRoot = true;
      this.#projectorMode = "replace";
      if (!this.shadowRoot) {
        this.attachShadow({ mode: "open", delegatesFocus: this.#delegatesFocus });
      }
      this.#initStylesheet(css);
      const div = document.createElement("div");
      this.shadowRoot.appendChild(div);
      requestAnimationFrame(() => {
        this.createProjector(div, (this as any).render);
      });
      window.addEventListener("updatecssapply", () => {
        this.#polyfillCssApply();
      });
    }
    
  }

  #reflectProperties() {
    this.#properties.filter((p) => p.reflect).forEach((p) => {
      if (p.type === "string" && this[p.name] !== undefined && this[p.name] !== null) {
        this.setAttribute(p.attribute, this[p.name]);
      } else if (p.type === "number" && this[p.name] !== undefined && this[p.name] !== null) {
        this.setAttribute(p.attribute, this[p.name].toString());
      } else if (p.type === "boolean" && this[p.name] === true) {
        if (this[p.name] === true && !this.hasAttribute(p.attribute)) {
          this.setAttribute(p.attribute, "");
        }
      }  else if (p.type === "boolean" && this[p.name] === false) {
        if (this.hasAttribute(p.attribute)) {
          this.removeAttribute(p.attribute);
        }
      } else if (p.type === "object" && this[p.name] !== undefined && this[p.name] !== null) {
        if (p.converter) {
          this.setAttribute(p.attribute, p.converter.toAttribute(this[p.name]));
        } else {
          this.setAttribute(p.attribute, JSON.stringify(this[p.name]));
        }
      }
    });
  }

  #initProperties = () => {
    const propertyNames = (this.constructor as any).__properties__;
    this.#properties = [];
    if (!propertyNames) {
      return;
    }
    this.#properties = propertyNames.map((propertyName) => {
      return { ...this[`__property_${propertyName}__`], name: propertyName };
    });
  };

  get #hasAdoptedStyleSheetsSupport(): boolean {
    return (
      Array.isArray(document.adoptedStyleSheets) &&
      "replace" in CSSStyleSheet.prototype
    );
  }

  #polyfillCssApply(): string {
    let style = replaceApplyToCssVars(this.#cssText);
    if (this.#cssText !== style) {
      this.#cssText = style;
      if (this.#hasAdoptedStyleSheetsSupport && this.#cssSheet) {
        this.#cssSheet.replaceSync(style);
      } else if (!this.#hasAdoptedStyleSheetsSupport) {
        if (this.#linkElement) {
          URL.revokeObjectURL(this.#linkElement.href);
        }
        this.#linkElement.href = URL.createObjectURL(
          new Blob([style], { type: "text/css" })
        );
      }
    }
    return style;
  }

  #initStylesheet(style: string) {
    this.#cssText = style;
    if (this.#useShadowRoot && this.shadowRoot) {
      this.addStylesheetToRootNode(style, this.shadowRoot);
    } else if (!this.#useShadowRoot) {
      this.addStylesheetToRootNode(style, document);
    }
  }

  #getHashCode(s: string) {
    for (var i = 0, h = 0; i < s.length; i++)
      h = (Math.imul(31, h) + s.charCodeAt(i)) | 0;
    return h;
  }

  protected get internals() {
    return this.#internals;
  }

  protected set internals(elementInternals: ElementInternals) {
    this.#internals = elementInternals;
  }

  protected addStylesheetToRootNode(
    style: string,
    rootNode: ShadowRoot | Document
  ) {
    if (this.#hasAdoptedStyleSheetsSupport) {
      if (!this.#cssSheet) {
        this.#cssSheet = new CSSStyleSheet();
      }
      style = this.#polyfillCssApply();
      this.#cssSheet.replaceSync(style);
      if (this.#isSheetAdopted) {
        return;
      }
      if (rootNode instanceof Document) {
        const styleHash = this.#getHashCode(style);
        if (documentAdoptedStyleSheets.indexOf(styleHash) === -1) {
          document.adoptedStyleSheets = [
            ...document.adoptedStyleSheets,
            this.#cssSheet,
          ];
          this.#isSheetAdopted = true;
          documentAdoptedStyleSheets.push(styleHash);
        }
      } else {
        this.shadowRoot.adoptedStyleSheets = [this.#cssSheet];
        this.#isSheetAdopted = true;
      }
    } else {
      this.#linkElement = document.createElement("link");
      this.#linkElement.rel = "stylesheet";
      style = this.#polyfillCssApply();
      this.#linkElement.href = URL.createObjectURL(
        new Blob([style], { type: "text/css" })
      );
      if (rootNode instanceof Document) {
        const styleHash = this.#getHashCode(style);
        if (documentAdoptedStyleSheets.indexOf(styleHash) === -1) {
          document.head.appendChild(this.#linkElement);
          documentAdoptedStyleSheets.push(styleHash);
        }
      } else {
        this.shadowRoot.appendChild(this.#linkElement);
      }
    }
  }

  protected templateFromString(
    template: string,
    useShadowRoot = true,
  ): DocumentFragment {
    const templateElement = document.createElement("template");
    templateElement.innerHTML = template;
    const fragmet = document.createDocumentFragment();
    fragmet.appendChild(templateElement.content);
    const styleElement = fragmet.querySelector("style");
    if (useShadowRoot) {
      this.#useShadowRoot = true;
      if (!this.shadowRoot) {
        this.attachShadow({ mode: "open", delegatesFocus: this.#delegatesFocus });
      }
    }
    this.#initStylesheet(styleElement.textContent);
    styleElement.remove();
    window.addEventListener("updatecssapply", () => {
      this.#polyfillCssApply();
    });
   
    return fragmet;
  }

  protected adoptStyle(
    root: Document | ShadowRoot,
    css: string
  ): string | void {
    this.addStylesheetToRootNode(css, root);
  }

  protected createProjector(
    element: Element,
    render: () => VNode
  ): Promise<Projector> {
    return new Promise<Projector>((resolve, reject) => {
      let projector: Projector;
      const mode = this.#projectorMode ? this.#projectorMode : "append";
      requestAnimationFrame(() => {
        projector = (window as any).Maquette.createProjector({
          performanceLogger: (eventName) => {
            if (eventName === "renderStart" || eventName === "renderDone") {
              this.#invokeRenderLifecycleFn(eventName);
            }
          }
        });
        projector[mode](element, render.bind(this));
        this.#projector = projector;
        this.#canReflect = true;
        this.#reflectProperties();
        projector.renderNow();
        resolve(projector);
        this.dispatchEvent(new CustomEvent("firstRender", {}));
      });
    });
  }

  scheduleRender(): void {
    this.#projector?.scheduleRender();
  }

  renderNow(): void {
    this.#projector?.renderNow();
  }

  connectedCallback() {
    this.#connected = true;
    this.#controllers.forEach((controller) => {
      controller?.connected();
    });
  }

  disconnectedCallback() {
    this.#connected = false;
    this.#controllers.forEach((controller) => {
      controller?.disconnected();
    });
  }

  attributeChangedCallback(
    name: string,
    oldValue: string | null,
    newValue: string | null
  ) {
    const prop = this.#properties.find((p) => p.attribute === name);
    if (!prop) {
      return;
    }

    if(prop.converter) {
      this[prop.name] = prop.converter.fromAttribute(newValue);
      return;
    }

    const type = prop.type;
    if (type === "string") {
      this[prop.name] = newValue;
    } else if (type === "number") {
      this[prop.name] = parseFloat(newValue);
    } else if (type === "boolean") {
      this[prop.name] = newValue !== null;
    } else if (type === "object") {
      this[prop.name] = JSON.parse(newValue);
    }
  }

  #isFirstRenderStart = true;

  #isFirstRenderDone = true;


  #invokeRenderLifecycleFn(eventName: string) {
    if (this[eventName]){
      this[eventName](eventName === "renderStart" ? this.#isFirstRenderStart : this.#isFirstRenderDone);
    }
    const controllerEventName = `host${eventName.charAt(0).toUpperCase()}${eventName.slice(1)}`;
    this.#controllers.forEach((controller) => {
      if (controller[controllerEventName]) {
        controller[controllerEventName](eventName === "renderStart" ? this.#isFirstRenderStart : this.#isFirstRenderDone);
      }
    });
    if (eventName === "renderStart") {
      this.#isFirstRenderStart = false;
    } else {
      this.#isFirstRenderDone = false;
    }
  }
}
