export interface HighlightableInterface {
  highlight: boolean;
}
import type { CustomElement } from "../../custom-element";

type Constructor<T> = new (...args: any[]) => T;

export const Highlightable =
  <T extends Constructor<CustomElement>>(superClass: T) => {
    class HighlightableElement extends superClass {
      get highlight() {
        return this.#highlight;
      }

      set highlight(value) {
        this.#highlight = value;
        let color = "unset";
        if  (value) {
          color = "yellow";
        }
        const div = this?.shadowRoot.querySelector("div");
        if (div) {
          div.style.backgroundColor = color;
        }
      }

      #highlight = false;
    }
    // ...and a helper render method:

    return HighlightableElement as Constructor<HighlightableInterface> & T;
  };