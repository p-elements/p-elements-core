import { Highlightable } from "./mixin/highlight";
import anime from "animejs";

import {customElementConfig as CustomElementConfig} from "../decorators/custom-element-config";
import {property as Property, AttributeConverter} from "../decorators/property";
import {propertyRenderOnSet as PropertyRenderOnSet} from "../decorators/render-property-on-set";
import {query as Query} from "../decorators/query";
import {CustomElement} from "../custom-element";
import {CustomElementController} from "../custom-element-controller";

@CustomElementConfig({
  tagName: "my-button",
})
export class MyButton extends CustomElement {
  static style = `.foo{background-color: red; color: white; border: 0px;}`;
  static delegatesFocus = true;
  render = () => {
    return <button class="foo"><slot></slot></button>;
  }
}

@CustomElementConfig({
  tagName: "my-greetings",
})
export class MyGreetings extends Highlightable(class extends CustomElement { }) {

  static isFormAssociated = true;

  private count: number = 1;

  @PropertyRenderOnSet
  private name: string = "";

  @PropertyRenderOnSet
  public foo: string = "foo";

  private onInput = (e) => {
    this.name = e.target.value;
  };

  @Query("#SlotContainer")
  private slotContainer: HTMLDivElement;

  private enterAnimation(domNode, properties) {
    anime({
      targets: domNode,
      translateX: [
        { value: 100, duration: 1200 },
        { value: 0, duration: 800 },
      ],
      rotate: "2turn",
      duration: 3000,
      loop: false,
    });
  }

  private exitAnimation(domNode, removeDomNodeFunction, properties) {
    anime({
      targets: domNode,
      translateX: [
        { value: 100, duration: 1200 },
        { value: 0, duration: 800 },
      ],
      duration: 3000,
      opacity: 0,
      loop: false,
      complete: () => {
        removeDomNodeFunction(domNode);
      },
    });
  }

  private countUpdateAnimation(domNode, properties, previousProperties) {
    const basicTimeline = anime.timeline();
    basicTimeline.add([
      {
        duration: 300,
        targets: domNode,
        translateX: 200,
        scale: 1.9,
        easing: "easeOutExpo",
      },
      {
        duration: 300,
        targets: domNode,
        translateX: 0,
        scale: 1,
        easing: "easeInExpo",
      },
    ]);
  }

  private onPeterClick = (e) => {
    console.log({ e, p: this });
  };

  private render = () => {
    return (
      <div>
        <input type="checkbox" checked={this.name === "Peter"} />
        <h1 on={{ "click": (e) => console.log(e, this.slotContainer) }} id="Foo">
          Hallo:
        </h1>

        <h1 onclick={() => console.log("click " + this.name)}
          data-title={this.name && this.name.toLocaleUpperCase()}
          class="title"
        >
          {this.foo} {this.name}
        </h1>
        <p data-count={this.count} updateAnimation={this.countUpdateAnimation}>
          {this.count}
        </p>
        <div id="SlotContainer">
          <slot></slot>
        </div>
        <p eventListeners={
          [
            ["mouseenter", () => console.log("enter"), {once: true}],
            ["mouseleave", () => console.log("leave"), {once: true}]
          ]
        }>
          Please enter your name (hint type{" "}
          <em>
            <strong>Peter</strong>
          </em>
          )<br />
          <input type="text" value={this.name} oninput={this.onInput} />
        </p>
        <p>
          <a is="super-a">Super a</a>
        </p>
        {this.name &&
          this.name.toLowerCase &&
          this.name.toLowerCase().indexOf("peter") > -1 ? (
          <div
            key="peter"
            afterRemoved={this.peterRemoved}
            afterCreate={this.peterCreate}
            class="is-peter"
            eventListeners={[["click", this.onPeterClick]]}
            enterAnimation={this.enterAnimation}
            exitAnimation={this.exitAnimation}
          >
            <p>Hello Peter</p>
            <img
              class="is-peter--image"
              src="https://s-media-cache-ak0.pinimg.com/474x/ce/1d/07/ce1d07011c0afb8e0614a0ae42a8c861.jpg"
              height="140"
            />
          </div>
        ) : (
          ""
        )}
      </div>
    );
  };

  private peterRemoved() {
    console.log("removed");
  }

  private peterCreate() {
    console.log("create");
  }

  connectedCallback() {
    let template = this.templateFromString(`
      <style>

        :host{
          /* display: none; */
        }

        .root {
          display: flex;
          padding: 10px;
          flex-direction: row;
        }

        #Foo {
          @apply --mixin-sample;
          text-decoration: underline;
        }

        #SlotContainer ::slotted(*) {
          font-weight: bold;
          color: green;
          font-size: 1.5em;
        }

      </style>
      <div class="root"></div>
    `);

    this.shadowRoot.appendChild(template);

    this.createProjector(this.shadowRoot.querySelector(".root"), this.render);

    setInterval(() => {
      this.count++;
      this.renderNow();
    }, 5000);

    window.addEventListener("foo", this.onFooEvent);
  }

  private onFooEvent = (e: CustomEvent) => {
    this.foo = e.detail.foo;
  };

  static get observedAttributes() {
    return ["name"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === "name") {
      this.name = newValue;
    }
  }
}

const stringArrayConverter: AttributeConverter<string[]> = {
  fromAttribute: (value) => {
    if (!value) {
      return null;
    }
    return value.split(",").map((v) => v.trim());
  },
  toAttribute: (value) => {
    if (!value.join) {
      return null;
    }
    return value.join(",");
  },
};

class DisabledController extends CustomElementController {

  #connected = false;

  @Property({ type: "boolean" })
  disabled = false;

  #updateAttributes() {
    if (!this.#connected) {
      return;
    }
    if (this.disabled === true) {
      this.hostElement.setAttribute("disabled", "!!");
    } else {
      this.hostElement.removeAttribute("disabled");
    }
  }

  connected() {
    console.log("connected");
    this.#connected = true;
    this.#updateAttributes();
  }

  disconnected() {
    console.info("disconnected");
    this.#connected = false;
  }

  shouldUpdate(property, oldValue, newValue) {
    console.info("shouldUpdate", { property, oldValue, newValue });
    return true;
  }

  updated(property, oldValue, newValue) {
    console.info("updated", { property, oldValue, newValue });
    this.#updateAttributes();
  }

  hostRenderStart(isFirstRender) {
    console.info("hostRenderStart", { isFirstRender, controller: this });
  }

  hostRenderDone(isFirstRender) {
    console.info("hostRenderDone", { isFirstRender, controller: this });
  }
}

@CustomElementConfig({
  tagName: "p-foo",
})
class PFoo extends CustomElement {

  static style = `.foo{color: red} .foo__disabled{opacity: 0.5;}`;

  init() {
    console.info("init");
  }

  disabledController: DisabledController = new DisabledController(this);

  @Property({ type: "string", attribute: "name", reflect: true })
  name: string;

  @Property({ type: "number", attribute: "age", reflect: true })
  age: number;

  @Property({ type: "object", attribute: "data", reflect: true })
  data: any;

  @Property({ attribute: "items", type: "object", reflect: true, converter: stringArrayConverter })
  items: string[] = ["foo", "bar"];

  @Property({ type: "string", attribute: "nick-name", readonly: true })
  nickName: string = "de prutser";

  shouldUpdate(property, oldValue, newValue) {
    if (property === "name" && typeof newValue === "string" && newValue.toLocaleLowerCase() === "adolf") {
      return false;
    }
    return true;
  }

  updated(property, oldValue, newValue) {
    console.info("PFoo", { name: property, old: oldValue, newValue });
  }

  static observedAttributes = ["disabled"];

  attributeChangedCallback(name, oldValue, newValue) {
    super.attributeChangedCallback(name, oldValue, newValue);
    if (name === "disabled") {
      this.disabledController.disabled = newValue !== null;
    }
  }

  renderStart(isFirstRender) {
    console.info("renderStart", { isFirstRender, tag: this.tagName });
  }

  renderDone(isFirstRender) {
    console.info("renderDone", { isFirstRender, tag: this.tagName });
  }

  render = () => {
    return <div classes={{ foo: true, "foo__disabled": this.disabledController.disabled }}>
      <div >Hello {this.name} {this.nickName ? <span> ({this.nickName})</span> : null}</div>
      <div>age {this.age}</div>
      {this.data ? <pre>{JSON.stringify(this.data, null, 2)}</pre> : null}
      {this.items?.map((item, i) => <div key={`item${i}`}>{item}</div>)}
    </div>;
  };

}

class SuperAnchorElement extends HTMLAnchorElement {
  constructor() {
    super();
    this.onclick = (e) => {
      e.preventDefault();
      alert("super");
    }
  }
  public connectedCallback() {
    this.classList.add("super");
    this.style.color = "red";
  }
}

window.customElements.define("super-a", SuperAnchorElement, { extends: "a" });
