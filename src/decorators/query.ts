export const query = (selector: string, useShadowRoot = true) => {
  return function(target: Object, propertyKey: string) {
    Object.defineProperty(target, propertyKey, {
      get: function() {
        if (useShadowRoot) {
          return this.shadowRoot.querySelector(selector);
        }
        return this.querySelector(selector);
      },
    });
  }
};
