type PropertyType = "string" | "number" | "boolean" | "object";

export interface AttributeConverter<T> {
  fromAttribute?(value: string | null): T;
  toAttribute?(value: T): string;
}

export interface PropertyOptions {
  type?: PropertyType;
  reflect?: boolean;
  attribute?: string;
  readonly?: boolean;
  converter?: AttributeConverter<any>;
}

export interface PropertyInfo extends PropertyOptions {
  name: string;
}

const typeToStringValue = <T>(type: PropertyType, value: T): string => {
  if (value === undefined) {
    return undefined;
  }
  else if (value === null) {
    return null;
  }
  switch (type) {
    case "string":
      return value.toString();
    case "number":
      return value.toString();
    case "object":
      return JSON.stringify(value);
  }
};

export const property = (propertyOptions: PropertyOptions) => {
  return (target: any, name: string) => {
    const ctor = target.constructor as any;
    const sym = Symbol(name + "_PropertyDecorator");
    if (ctor && !ctor.observedAttributes) {
      ctor.observedAttributes = [];
    }
    if (ctor && !ctor.__properties__) {
      ctor.__properties__ = [];
    }
    ctor.__properties__.push(name);
    if (
      propertyOptions.attribute &&
      !propertyOptions.readonly &&
      ctor.observedAttributes.indexOf(propertyOptions.attribute) === -1
    ) {
      ctor.observedAttributes.push(propertyOptions.attribute);
    }
    Object.defineProperty(target, `__property_${name}__`, {
      configurable: true,
      enumerable: true,
      get: function () {
        return propertyOptions;
      },
    });

    if (propertyOptions.readonly === true) {
      Object.defineProperty(target, name, {
        get: function () {
          return this[sym];
        },
        set: function (value) {
          this[sym] = value;
          this?.scheduleRender();
          delete this[name];
          Object.defineProperty(this, name, {
            configurable: true,
            enumerable: true,
            get: function () {
              return this[sym];
            },
            set: function () {
              throw new Error("Cannot set read-only property");
            },
          });
          Object.seal(this[sym]);
        },
      });
      return;
    }

    Object.defineProperty(target, name, {
      configurable: true,
      enumerable: true,
      get: function () {
        return this[sym];
      },
      set: function (value) {
        const oldValue = this[sym];
        const skipSetValue = this?.shouldUpdate && !this.shouldUpdate(name, oldValue, value);
        const elementToSetAttribute = this.isCustomElementController ? this.hostElement : this;
        const attributeValue = propertyOptions.converter
        ? propertyOptions.converter.toAttribute(value)
        : typeToStringValue(propertyOptions.type, value);
        if (skipSetValue) {
          this[sym] = oldValue;
          requestAnimationFrame(() => {
            elementToSetAttribute.setAttribute(propertyOptions.attribute, oldValue)
          });
        } else {
          this[sym] = value;
        }
        const handleChange = () => {
          if (this[sym] !== oldValue) {
            this?.scheduleRender && this.scheduleRender();
            this?.updated && this.updated(name, oldValue, this[sym]);
          }
        };
        if (propertyOptions.reflect && propertyOptions.attribute && elementToSetAttribute._canReflect) {
          if (propertyOptions.type !== "boolean") {
            if (!value) {
              elementToSetAttribute.removeAttribute(propertyOptions.attribute);
              handleChange();
              return;
            }
            if (
              elementToSetAttribute.getAttribute(propertyOptions.attribute) === attributeValue
            ) {
              handleChange();
              return;
            }
            if (attributeValue !== null) {
              elementToSetAttribute.setAttribute(propertyOptions.attribute, attributeValue);
            } else {
              elementToSetAttribute.removeAttribute(propertyOptions.attribute);
            }
          } else {
            if (value && !elementToSetAttribute.hasAttribute(propertyOptions.attribute)) {
              elementToSetAttribute.setAttribute(propertyOptions.attribute, "");
            } else if (!value && elementToSetAttribute.hasAttribute(propertyOptions.attribute)) {
              elementToSetAttribute.removeAttribute(propertyOptions.attribute);
            }
            handleChange();
            return;
          }
        }
        handleChange();
        this?.scheduleRender && this.scheduleRender();
      },
    });
  };
};

