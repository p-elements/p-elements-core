export interface IElementConfig {
  tagName: string;
  options?: {
    extends: string;
  };
}
export const customElementConfig = (config: IElementConfig) => {
  return (Element) => {
    if (customElements.get(config.tagName)) {
      console.warn(
        `Custom element with tag name ${config.tagName} already exists.`
      );
      return;
    }
    customElements.define(config.tagName, Element, config.options);
  };
};
