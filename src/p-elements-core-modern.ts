import "./custom-style-element";

import * as _ from "underscore";
import * as anime from "animejs";

import { bind as Bind } from "./decorators/bind";
import { customElementConfig as CustomElementConfig } from "./decorators/custom-element-config";
import { query as Query } from "./decorators/query";
import { property as Property } from "./decorators/property";
import { propertyRenderOnSet as PropertyRenderOnSet } from "./decorators/render-property-on-set";
import { CustomElement } from "./custom-element";
import { CustomElementController } from "./custom-element-controller";
import { createCache } from "./maquette/cache";
import { createMapping } from "./maquette/mapping";
import { createProjector } from "./maquette/projector";
import { dom } from "./maquette/dom";
import { jsx } from "./maquette/jsx";

const Maquette = {
  createCache,
  createMapping,
  createProjector,
  dom,
  h: jsx,
};


((window: any) => {
  window._ = _;
  window.Maquette = Maquette;
  window.h = jsx;
  window.anime = anime;
  window.CustomElement = CustomElement;
  window.CustomElementController = CustomElementController;
  window.CustomElementConfig = CustomElementConfig;
  window.PropertyRenderOnSet = PropertyRenderOnSet;
  window.RenderOnSet = PropertyRenderOnSet;
  window.Property = Property;
  window.Query = Query;
  window.Bind = Bind;
})(window);

const h = Maquette.h;
const RenderOnSet = PropertyRenderOnSet;

export {
  _,
  h,
  CustomElement,
  CustomElementController,
  CustomElementConfig,
  Maquette,
  PropertyRenderOnSet,
  RenderOnSet,
  Property,
  Query,
  Bind,
};
