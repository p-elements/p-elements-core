import { cssApplyToCssVars, cssApplyVars } from "./helpers/css";

export class CustomStyleElement extends HTMLLinkElement {

  static cssApplyVars = cssApplyVars;

  public connectedCallback() {
    let url = this.getAttribute("href");
    fetch(url).then((response) => {
      response.text().then((text) => {
        const cssText = cssApplyToCssVars(text);
        if (!cssText) {
          return;
        }
        const linkElement = document.createElement("link");
        linkElement.rel = "stylesheet";
        linkElement.href = URL.createObjectURL(
          new Blob([cssText], { type: "text/css" })
        );
        this.parentElement.replaceWith(linkElement);
        window.dispatchEvent(new CustomEvent("updatecssapply"));
      });
    });
  }
}

customElements.whenDefined("custom-style").then(() => {
  document.body.parentElement.classList.add("custom-style-defined");
});

customElements.define("custom-style", CustomStyleElement, { extends: "link" });