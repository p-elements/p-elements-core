import { CustomStyleElement } from "../custom-style-element";

const VAR_ASSIGN =
  /(?:^|[;\s{]\s*)(--[\w-]*?)\s*:\s*(?:((?:'(?:\\'|.)*?'|"(?:\\"|.)*?"|\([^)]*?\)|[^};{])+)|\{([^}]*)\}(?:(?=[;\s}])|$))/gi;

export const cssApplyVars = new Map<string, string[]>();

function getMixinValues(valueMixin: string): { key: string; value: string }[] {
  return valueMixin
    ?.split(";")
    .map((mixin: string) => {
      const [key, value] = mixin.split(":", 2);
      return { key: key.trim(), value: value ? value.trim() : "" };
    })
    .filter(
      (mixin: any) =>
        mixin.key && mixin.key !== "" && mixin.value !== ""
    );
}

export function cssApplyToCssVars(style: string): string {
  let nrOfReplacements = 0;
  const cssText = style.replace(
    VAR_ASSIGN,
    (matchText, propertyName, valueProperty, valueMixin) => {
      if (valueMixin) {
        nrOfReplacements++;
        const values = getMixinValues(valueMixin);
        let replaceText = "";
        const props: string[] = [];
        values.forEach((mixin: any) => {
          props.push(mixin.key);
          replaceText +=
            `${propertyName}_-_${mixin.key}: ${mixin.value};` + "\n";
        });
        cssApplyVars.set(propertyName, props);
        return matchText.replace(
          propertyName,
          `${replaceText}
        ${propertyName}`
        );
      }
      return matchText;
    }
  );
  return nrOfReplacements > 0 ? cssText : null;
}

export function replaceApplyToCssVars(cssText: string) : string {
  const allVars = (
    customElements.get("custom-style") as typeof CustomStyleElement
  ).cssApplyVars;

  let style = cssText;
  const MIXIN_MATCH = /(?:^|\W+)@apply\s*\(?([^);\n]*)\)?/gi;
  let m;
  while ((m = MIXIN_MATCH.exec(style))) {
    let matchText = m[0];
    let mixinName = m[1];
    if (allVars.has(mixinName)) {
      const searchText = "@apply" + matchText.split("@apply", 2)[1];
      const replaceText = allVars
        .get(mixinName)
        .map((prop: string) => `;${prop}: var(${mixinName}_-_${prop});`)
        .join("");

      style = style.replace(searchText, replaceText);
    }
  }
  return style;
}
