import type { CustomElement } from './custom-element';

export interface ICustomElementController {
  renderNow(): void;
  init?: () => void;
  connected? : () => void;
  disconnected? : () => void;
  hostRenderStart?: () => void;
  hostRenderDone? : () => void;
  hostElement: CustomElement;
}
export abstract class CustomElementController implements ICustomElementController {
  constructor(hostElement: CustomElement) {
    this.#hostElement = hostElement;
    this.#hostElement.addController(this);
  }

  #hostElement: CustomElement;

  get hostElement() {
    return this.#hostElement;
  }

  renderNow() {
    this.hostElement?.renderNow();
  }

  scheduleRender() {
    this.hostElement?.scheduleRender();
  }
}
