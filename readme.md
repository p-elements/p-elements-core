# P-Element

## CustomElement base class

### Static properties

#### `projectorMode` "append", "merge" or "replace"

The projector mode when using `templateFromString` function

#### `style` string

The style is adopted to the rootNode.
if this is set there is no need for calling `templateFromString`. A root node is created and replaced with the result of the `render` function.

example

``` typescript
class MyElement extends CustomElement {
  static style = `
  :host {
    color: red;
  }`;

  render = () :VNode {
    return <div>Hello world</div>;
  }
}
```

#### `isFormAssociated` boolean

If true, elementInternals are created using attachInternals in the constructor.

example

``` typescript
class MyElement extends CustomElement {

  static isFormAssociated = true;

  @Property({type: "string", attribute: "value", reflect: true })
  value: string = "Peter" ;

  updated(property: string, oldValue: any, newValue: any) {
    if (property === "value"){
      this.internals.setFormValue(this.value);
    }
  }

  render = () :VNode {
    return <div>Hello {this.value}.</div>;
  }
}
```

### Decorators

#### `@CustomElementConfig`

Use this decorator to define the custom element

example

``` typescript
@CustomElementConfig({
  tagName: "p-foo",
})
class PFooElement extends CustomElement {
  ...
}
```

If you want to extend a build in component you need to specify the buildin.

example

``` typescript
@CustomElementConfig({
    tagName: "super-a",
    options: {
      extends: "a",
    },
  })
class SuperAnchorElement extends HTMLAnchorElement {
  ...
}
```

#### `@Property`

Use the `@Property` decorator to define reactive properties.

example

``` typescript
@CustomElementConfig({
  tagName: "p-foo",
})
class PFooElement extends CustomElement {
  ...

  @Property({type: "string", attribute: "nick-name", reflect: true})
  nickName: string;

  attributeChangedCallback(name: string, oldValue: string, newValue: string): void {
    super.attributeChangedCallback(name, oldValue, newValue);
    ...
  }
```
The `type` option could be  "string", "number", "boolean" or "object".

If `attribute` option is set the property will be set on attribute change. If you implement a `attributeChangeCallback` you need to call the `super.attributeChangedCallback`.

If `reflect` is `true` the attribute is set on property change.

Use `converter` option to convert attribute to property

``` typescript
const stringArrayConverter: AttributeConverter<string[]> = {
  fromAttribute: (value) => {
    if (!value) {
      return null;
    }
    return value.split(",").map((v) => v.trim());
  },
  toAttribute: (value) => {
    if (!value.join) {
      return null;
    }
    return value.join(",");
  },
};

class MyElement extend CustomElement {
  @Property({attribute: "items", reflect: true, converter: stringArrayConverter})
  items: string[] = ["foo", "bar"];
}
```
#### `@Query` decorator

Use the `@Query` decorator to define a property that returns an element from the shadow dom.

example

``` typescript
class PFooElement extends CustomElement {
  ...
  @Query("#MyInput")
  nameInput: HTMLInputElement;

  render = () => {
    return <div>
      <label for="MyInput"></label>
      <input type="text" id="MyInput" />
    </div>;
  }
}
```

#### `@PropertyRenderOnSet` `@RenderOnSet`

Properties decorated with `@PropertyRenderOnSet` or `@RenderOnSet` call renderNow when setting a value.

example

``` typescript
class PFooElement extends CustomElement {
  ...
  @PropertyRenderOnSet
  public foo: string = "foo";
}
```

#### `@Bind`
(deprecated, use arrow function `myFn = () => console.log('myFn')`)

Functions decorated with `@Bind` will be replaced with the result of bind(this) on the function.


### Hooks

#### `init`

The init is the first function called after initialising of the custom element

#### `shouldUpdate`

Before a property is set to a new value

#### `updated`

After a property value is set to a new value

#### `renderStart`

When the custom element start the rendering 

#### `renderDone`

When the custom element is finished rendering

#### `attributeChangedCallback`

Call `super.attributeChangedCallback()` first.

